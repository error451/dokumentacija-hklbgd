# Dobrodošli!

Ovo je dokumentacija [HKLBGD-a](https://hklbgd.org/), Beogradskog hakerspejsa[^1].
[^1]: izgleda da uopšte nemamo na srpskoj vikipediji članak o haklabu, bilo bi lepo kada bi neko to napravio

Haklab Beograd (8-am){oos^m} je kreativni prostor otvoren za sve programere i entuzijaste tehnologije, umetnike i sve zainteresovane za ukrstanje tehnologije i umetnosti.

Haklab nije samo za zajednicu zainteresovanu za bezbednost racunara, kao sto bi se moglo protumaciti iz naziva.

Mozete doci i raditi na soptstvenim projektima u drustvu, organizovati ili pridruziti se radionici, hakatonu, game jam-u ili LAN partiju.

Ako ste zainteresovani za aktivnosti u hklbgd-u proverite "ŠTA I KAD" stranicu ili mailing listu. Osim regularnih radionica hklbgd je otvoren za sve.

### sadrzaja
sajt
lista servisa 
aktuelne projekti
arhiva projekta
trenutno raspolozivi resursi
zapisnici sa sastanaka
prezentacije sa radionica
slike sa okupljanja
dokumentacija haklabove mreže

### kanali komunikacije
- hklbgd.org na sajtu i link ka dokumentaciji
- mejling lista
- forum
- chat grupe wire, signal, ...
- drustvene mreze linkovi 

# Testiranje/Pesak
go to -> [[Stranica 2]]

- [ ] proveriti kod koga je login za hklbgd@tutanota.com
- [x] testranje check liste

ovde
^oznaka

#test-tag

[`button`](#+path){ #+path }

```yaml
neki kod # (1)! 
```
1. ovo je samo test [[Stranica 2]]

:hamster:

``` mermaid
graph LR
  A[Start] --> B{Error?};
  B -->|Yes| C[Hmm...];
  C --> D[Debug];
  D --> B;
  B ---->|No| E[Yay!];
```

ext:
![hklbgd](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse4.mm.bing.net%2Fth%3Fid%3DOIP.w1ZWsM7sPSyOeNKpQcwcAAHaCe%26pid%3DApi&f=1&ipt=37d16557d296de0539211b79d673fff7e82c51d56d2a2b24666f92753868526c&ipo=images)

int:
![[Pasted image 20221201233101.png]]

